'use strict';
const {Model, DataTypes} = require('sequelize');

class Filter extends Model {

  static fields = {
    name: DataTypes.STRING,
    type: DataTypes.STRING
  }

  static getAssociations(models) {
    return {
    }
    // define association here
  }
}


module.exports = Filter

