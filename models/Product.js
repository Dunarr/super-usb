'use strict';
const {Model, DataTypes} = require('sequelize');

class Product extends Model {

  static fields = {
    name: DataTypes.STRING,
    price: DataTypes.INTEGER,
    picture: DataTypes.STRING,
    description: DataTypes.TEXT,
    stock: DataTypes.INTEGER
  }

  static getAssociations(models) {
    return {
      carts: {relation: "hasMany", model: models.CartProduct},
      color: {relation: "belongsTo", model: models.Filter},
      size: {relation: "belongsTo", model: models.Filter},
      brand: {relation: "belongsTo", model: models.Filter}
    }
  }
}


module.exports = Product

