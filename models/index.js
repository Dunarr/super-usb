'use strict';
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const config = require(__dirname + '/../config/config.js');
const db = {};


const sequelize = new Sequelize(config.url, {
  define: {
    timestamps: false
  }
});


fs
.readdirSync(__dirname)
.filter(file => {
  return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
})
.forEach(file => {
  const model = require(path.join(__dirname, file));
  model.init(model.fields, {
    sequelize, modelName: model.name, underscored: true
  })
  db[model.name] = model;
});

Object.keys(db).forEach(modelName => {
  if (db[modelName].getAssociations) {
    const associations = Object.entries(db[modelName].getAssociations(db));
    associations.forEach(([field, association]) => {

      db[modelName][association.relation](association.model, {
        as: field
      })
    })
  }
});

db.sequelize = sequelize;

module.exports = db;
