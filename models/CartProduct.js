'use strict';
const {Model, DataTypes} = require('sequelize');

class CartProduct extends Model {

  static fields = {
    cartId: DataTypes.INTEGER,
    qty: DataTypes.INTEGER
  }

  static getAssociations(models) {
    return {
      product: {relation: "belongsTo", model: models.Product}
    }
    // define association here
  }
}


module.exports = CartProduct

