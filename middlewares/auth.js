const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  if(req.method === "OPTIONS") {
    next()
    return
  }
  try {
    const token = req.headers.authorization; //Vient récupérer le token dans headers authorizations, coupe l'infos à l'espace, créer un tableau avec le split puis on récup l'entrée du token qui est en 2eme pos [1]
    const decodedToken = jwt.verify(token, "SUPER_SECRET_PASSWORD");
    const userId = decodedToken.userId;
    if (!userId) {
      throw 'Invalid user ID';
    } else {
      req.userId = userId
      next();
    }
  } catch {
    res.status(401).json({
      error: "Invalid token"
    })
  }
};