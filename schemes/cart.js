module.exports = {
  include: ['product', 'qty'],
  assoc: {
    product: {
      include: ["id", "name", "price", "picture", "stock"]
    }
  }
}