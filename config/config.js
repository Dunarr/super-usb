const path = require("path");
require('dotenv').config()
module.exports = {
  url:"sqlite:"+path.join(__dirname, "..", "database", "db.sqlite"),
  }