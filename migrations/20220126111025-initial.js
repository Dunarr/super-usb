'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {

    await queryInterface.createTable('filters', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        allowNull: true,
        type: Sequelize.STRING
      },
      type: {
        allowNull: true,
        type: Sequelize.STRING
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
    })
    await queryInterface.createTable('products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        allowNull: true,
        type: Sequelize.STRING
      },
      price: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      picture: {
        allowNull: true,
        type: Sequelize.STRING
      },
      description: {
        allowNull: true,
        type: Sequelize.STRING
      },
      stock: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      color_id: {
        allowNull: true,
        type: Sequelize.UUID,
        references: {
          model: 'Filter', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
      },
      size_id: {
        allowNull: true,
        type: Sequelize.UUID,
        references: {
          model: 'Filter', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
      },
      brand_id: {
        allowNull: true,
        type: Sequelize.UUID,
        references: {
          model: 'Filter', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
      },

      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
    });
    return queryInterface.createTable(
      'CartProducts',
      {
        Product_id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
        },
        cart_id: {
          type: Sequelize.STRING,
          primaryKey: true,
        },
        qty: {
          type: Sequelize.INTEGER,
        },

        created_at: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: false,
          type: Sequelize.DATE
        },
      }
    );
  },


  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
}

