//importation d'express
const express = require('express');
//application d'express à une var pour pouvoir s'en servir
const app = express();
//importation de mongodb



require("./models")
//va chercher la logique des routes dans le fichier nous servant à mutualiser les routes ensemble
const productsRoutes = require('./routes/products');
const filtersRoutes = require('./routes/filters');
const specsRoutes = require('./routes/specs');
const sessionsRoutes = require('./routes/session');
const cartRoutes = require('./routes/cart');

const auth = require("./middlewares/auth")



//Permet au server de modifier des choses sur notre site ? CORS
app.use(express.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

//utilise le'routeur' de express.Router() de par exemple: ./routes/sauces.js
app.use('/api/products', productsRoutes);
app.use('/api/filters', filtersRoutes);
app.use('/api/session', sessionsRoutes);
app.use('/api/cart', auth, cartRoutes);
app.use('/', specsRoutes);

module.exports = app;