const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

router.post('/', (req, res, next) => {
  const userId = Math.floor(Math.random() * 90000) + 10000
  const token = jwt.sign( // on génère un token de session pour le user maintenant connecté
    {userId}, "SUPER_SECRET_PASSWORD", {expiresIn: '24h'})
  res.status(200).json({"api-token": token})
});

module.exports = router;