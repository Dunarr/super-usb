const express = require('express');
const router = express.Router();
const {CartProduct, Product} = require('../models')
const {Op} = require("sequelize");

const Serializer = require('sequelize-to-json')
const cartScheme = require("../schemes/cart")

router.get('/', async (req, res, next) => {
  let cartProducts = await CartProduct.findAll({where: {cartId:req.userId, qty:{[Op.gt]: 0}}, include: ["product"]})
  res.status(200).json(Serializer.serializeMany(cartProducts, CartProduct, cartScheme))

})
router.post('/', async (req, res, next) => {

  const productId = req.body.product
  const qty = req.body.qty
  if (!productId) {
    res.status(400).json({error: "missing parameter 'productId'"})
    return
  }
  if (!qty) {
    res.status(400).json({error: "missing parameter 'qty'"})
    return
  }
  try{

    const product = await Product.findByPk(productId)
    if(!product){
      res.status(200).json({message: "Produit inconnu"})
      return
    }

    let cartProduct = await CartProduct.findOne({where: {cartId:req.userId, ProductId:productId}})
    if(!cartProduct){



      cartProduct = await CartProduct.create({
        qty: 0,
        cartId: req.userId,
        productId: productId
      })

    }
    cartProduct.qty+= qty

    if(product.stock < cartProduct.qty){
      res.status(200).json({message: "Stock insuffisant"})
      return
    }


    await cartProduct.save()
    res.status(200).json({message: "Produit ajouté au pannier"})
  } catch (e) {
    console.log(e)
  }



});

router.put('/', async (req, res, next) => {

  const productId = req.body.product
  const qty = req.body.qty
  if (!productId) {
    res.status(400).json({error: "missing parameter 'productId'"})
    return
  }
  if (!qty) {
    res.status(400).json({error: "missing parameter 'qty'"})
    return
  }
  try{


    let cartProduct = await CartProduct.findOne({where: {cartId:req.userId, ProductId:productId}})
    if(!cartProduct){
      res.status(200).json({message: "Le produit n'est pas dans le pannier"})
      return
    }
    cartProduct.qty = qty


    if(cartProduct.stock < cartProduct.qty){
      res.status(200).json({message: "Stock insuffisant"})
      return
    }


    await cartProduct.save()
    res.status(200).json({message: "Quantité modifiée"})
  } catch (e) {
    console.log(e)
  }



});

module.exports = router;