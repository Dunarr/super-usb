const express = require('express');
const router = express.Router();
const {Filter} = require("../models")
const {Op} = require("sequelize");

const Serializer = require('sequelize-to-json')
const filterListScheme = require("../schemes/filter-list")

router.get('/', (req, res, next) => {

  Promise.all([
    Filter.findAll({where: {type:{[Op.eq]: 'color'}}}),
    Filter.findAll({where: {type:{[Op.eq]: 'size'}}}),
    Filter.findAll({where: {type:{[Op.eq]: 'brand'}}}),
  ])
  .then(([colorFilters, sizeFilters, brandFilters]) => {
    res.status(200).json({
      color: Serializer.serializeMany(colorFilters, Filter, filterListScheme),
      size: Serializer.serializeMany(sizeFilters, Filter, filterListScheme),
      brand: Serializer.serializeMany(brandFilters, Filter, filterListScheme)
    })
  }).catch(console.log)
});

module.exports = router;