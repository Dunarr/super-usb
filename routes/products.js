const express = require('express');
const router = express.Router();
const {Product} = require("../models")
const {Op} = require("sequelize");

const Serializer = require('sequelize-to-json')
const productListScheme = require("../schemes/product-list")

router.get('/', (req, res, next) => {
  const filters = {
    where: {
    }
  }
  if (req.query.search) {
    filters.where[Op.or] = [{name: {[Op.like]: "%"+req.query.search+"%"}},{description: {[Op.like]: "%"+req.query.search+"%"}}]
  }
  if (req.query.color) {
    filters.where.color_id = {[Op.eq]: req.query.color}
  }
  if (req.query.size) {
    filters.where.size_id = {[Op.eq]: req.query.size}
  }
  if (req.query.brand) {
    filters.where.brand_id = {[Op.eq]: req.query.brand}
  }
  Product.findAll(filters)
  .then(products => {
    res.status(200).json({results: Serializer.serializeMany(products, Product, productListScheme)})
  }).catch(console.log)
});

router.get('/:id', (req, res, next) => {
  Product.findByPk(req.params.id, {include: ["color", "brand", "size"]})
  .then(product => {
    res.status(200).json({
      id: product.id,
      name: product.name,
      price: product.price,
      picture: product.picture,
      description: product.description,
      color: product.color?.name || null,
      size: product.size?.name || null,
      brand: product.brand?.name || null,
      stock: product.stock
    })
  }).catch(console.log)
});


module.exports = router;