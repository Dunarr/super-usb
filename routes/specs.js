const express = require('express');
const router = express.Router();
const fs = require("fs");
const path = require("path");
const specs = fs.readFileSync(path.join(__dirname, "..", "specs.md"))

router.get('/', (req, res, next) => {
  res.status(200).send(specs)
});


module.exports = router;